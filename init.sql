CREATE DATABASE IF NOT EXISTS hyve;
USE hyve;
CREATE TABLE IF NOT EXISTS contacts (id int NOT NULL,title tinytext CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL, first_name tinytext CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL,last_name tinytext CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL,email tinytext NOT NULL,email_domain_ip tinytext NOT NULL, note text CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL, timezone tinytext NOT NULL,timestamp timestamp NOT NULL) ENGINE=InnoDB DEFAULT CHARSET=utf8mb3;

ALTER TABLE contacts
  ADD PRIMARY KEY (id);
COMMIT;

CREATE USER 'user'@'localhost' IDENTIFIED WITH mysql_native_password BY 'secret';
GRANT ALL PRIVILEGES ON hyve.* TO 'user'@'localhost' WITH GRANT OPTION;

FLUSH PRIVILEGES;